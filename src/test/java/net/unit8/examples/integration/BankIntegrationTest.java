package net.unit8.examples.integration;

import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

class BankIntegrationTest {
    @Test
    void noGuard() {
        Configuration.pageLoadStrategy = "none";
        open("/bank/");
        $("#link-to-no_guard-tx").sendKeys(Keys.CONTROL, Keys.RETURN);
        $("#link-to-no_guard-tx").sendKeys(Keys.CONTROL, Keys.RETURN);
        switchTo().window(1);
        $("#amount").setValue("8000");
        switchTo().window(2);
        $("#amount").setValue("8000");
        switchTo().window(1);
        $("#button-withdraw").click();
        switchTo().window(2);
        $("#button-withdraw").click();
        sleep(300_000);
    }
}
