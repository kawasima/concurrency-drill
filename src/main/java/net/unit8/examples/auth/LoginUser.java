package net.unit8.examples.auth;

import net.unit8.examples.ec.Customer;
import org.springframework.security.core.userdetails.User;

import java.util.Set;

public class LoginUser extends User {
    public LoginUser(Customer customer) {
        super(customer.getEmail(), customer.getPassword(), true, true, true, true, Set.of());
    }
}
