package net.unit8.examples.ec;

public interface CartCleaner {
    void clean(Cart cart);
}
