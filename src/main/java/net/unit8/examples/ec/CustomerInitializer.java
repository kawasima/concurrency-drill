package net.unit8.examples.ec;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

@Component
public class CustomerInitializer implements InitializingBean {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private TransactionTemplate tx;
    @Override
    public void afterPropertiesSet() throws Exception {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setEmail("user@example.com");

        Cart cart = new Cart();
        cart.setCustomer(customer);

        tx.execute(status -> {
            customerRepository.save(customer);
            cartRepository.save(cart);
            return null;
        });
    }
}
