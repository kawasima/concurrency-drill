package net.unit8.examples.ec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("orderIdempotencyGuarantor")
public class OrderIdempotencyGuarantor implements IdempotencyGuarantor<Order> {
    @Autowired
    private OrderRepository orderRepository;
    @Override
    public Optional<Order> validate(String idempotentKey) {
        if (idempotentKey == null) {
            return Optional.empty();
        }
        return orderRepository.findByIdempotentKey(idempotentKey);
    }
}
