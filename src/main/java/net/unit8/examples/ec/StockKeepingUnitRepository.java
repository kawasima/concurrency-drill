package net.unit8.examples.ec;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StockKeepingUnitRepository extends JpaRepository<StockKeepingUnit, Long> {
}
