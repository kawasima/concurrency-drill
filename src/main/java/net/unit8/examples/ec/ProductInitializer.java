package net.unit8.examples.ec;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class ProductInitializer implements InitializingBean {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TransactionTemplate tx;

    private static StockKeepingUnit sku(String name, long price) {
        StockKeepingUnit sku = new StockKeepingUnit();
        sku.setName(name);
        sku.setPrice(price);
        return sku;
    }

    private static Product product(String name, String description, StockKeepingUnit... skus) {
        Product product = new Product();
        product.setName(name);
        product.setDescription(description);
        Arrays.stream(skus).forEach(sku -> sku.setProduct(product));
        product.setStockKeepingUnits(Arrays.asList(skus));
        return product;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Product product1 = product("Tシャツ", "ふつうのTシャツです",
                sku("S", 1900L),
                sku("M", 1900L),
                sku("L", 1900L)
        );

        Product product2 = product("帽子", "色の色々ある帽子です。",
                sku("赤", 2400L),
                sku("青", 2400L),
                sku("黒", 2400L)
        );

        tx.execute(status -> {
            productRepository.save(product1);
            productRepository.save(product2);
            return null;
        });
    }
}
