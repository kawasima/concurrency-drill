package net.unit8.examples.ec;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@WebAdapter
@Controller
@RequestMapping("/ec/admin/products")
public class ProductAdminController {
    private ProductRepository productRepository;

    @ModelAttribute("form")
    public ProductRegistrationForm createForm() {
        return new ProductRegistrationForm();
    }

    @RequestMapping("")
    public String index(Model model) {
        PageRequest pageRequest = PageRequest.of(0, 10);
        Page<Product> products = productRepository.findAll(pageRequest);
        model.addAttribute("products", products);
        return "ec/admin/products/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String newForm() {
        return "ec/admin/products/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String register(ProductRegistrationForm form) {
        Product product = new Product();
        product.setName(form.getName());
        product.setDescription(form.getDescription());

        return "ec/admin/products/complete";
    }
}
