package net.unit8.examples.ec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("cartCleanerByRecreation")
public class CartCleanerByRecreation implements CartCleaner {
    @Autowired
    private CartRepository cartRepository;

    @Override
    public void clean(Cart cart) {
        Customer customer = cart.getCustomer();
        cartRepository.delete(cart);
        cartRepository.flush();

        Cart newCart = new Cart();
        newCart.setCustomer(customer);
        cartRepository.save(newCart);
        cartRepository.flush();
    }
}
