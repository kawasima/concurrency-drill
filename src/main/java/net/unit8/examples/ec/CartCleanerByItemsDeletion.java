package net.unit8.examples.ec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("cartCleanerByItemsDeletion")
public class CartCleanerByItemsDeletion implements CartCleaner{
    @Autowired
    private CartItemRepository cartItemRepository;

    @Override
    public void clean(Cart cart) {
        cart.getItems().forEach(item -> {
            cartItemRepository.delete(item);
        });
    }
}
