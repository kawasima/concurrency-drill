package net.unit8.examples.ec;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@WebAdapter
@Controller
@RequestMapping("/ec/cart")
public class CartController {
    private static final Long CUSTOMER_ID = 1L;
    @Autowired
    private StockKeepingUnitRepository stockKeepingUnitRepository;
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CartItemRepository cartItemRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    @Qualifier("cartCleanerByItemsDeletion")
//    @Qualifier("cartCleanerByRecreation")
    private CartCleaner cartCleaner;

    @Autowired(required = true)
//    @Qualifier("orderIdempotencyGuarantor")
    @Qualifier("nonIdempotencyGuarantor")
    private IdempotencyGuarantor<Order> idempotencyGuarantor;

    @Autowired
    private TransactionTemplate tx;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public String show(Model model) {
        List<CartItem> cartItems = cartRepository.findByCustomerId(CUSTOMER_ID)
                .map(Cart::getItems)
                .orElseThrow();
        model.addAttribute("cartItems", cartItems);
        model.addAttribute("idempotentKey", UUID.randomUUID().toString());
        return "ec/cart/show";
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public String addToCart(@SessionAttribute("addItemToCartForm") AddItemToCartForm form) {
        Cart cart = cartRepository.findByCustomerId(CUSTOMER_ID).orElseThrow();

        CartItem cartItem = cart.getItems().stream()
                .filter(item -> item.getStockKeepingUnit().getId().equals(form.getSkuId())).findAny()
                .map(item -> {
                    item.setAmount(item.getAmount() + form.getAmount());
                    return item;
                }).orElseGet(() -> {
                            CartItem item = new CartItem();
                            item.setCart(cart);
                            item.setAmount(form.getAmount());
                            StockKeepingUnit sku = stockKeepingUnitRepository.findById(form.getSkuId()).orElseThrow();
                            item.setStockKeepingUnit(sku);
                            return item;
                        }
                );

        tx.execute(status -> {
            cartItemRepository.save(cartItem);
            return null;
        });
        return "redirect:items";
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public String checkout(@RequestParam(name = "idempotentKey", required = false) String idempotentKey, Model model) {
        Cart cart = cartRepository.findByCustomerId(CUSTOMER_ID).orElseThrow();
        Order order = idempotencyGuarantor.validate(idempotentKey).orElse(new Order());
        model.addAttribute("order", order);
        if (order.getId() != null) {
            return "ec/order/complete";
        }
        if (cart.getItems().isEmpty()) {
            throw new IllegalStateException("Cart is empty");
        }
        order.setIdempotentKey(idempotentKey);
        order.setCustomer(cart.getCustomer());
        order.setOrderLines(cart.getItems().stream()
                .map(item -> {
                    OrderLine orderLine = new OrderLine();
                    orderLine.setOrder(order);
                    orderLine.setAmount(item.getAmount());
                    orderLine.setStockKeepingUnit(item.getStockKeepingUnit());
                    return orderLine;
                })
                .collect(Collectors.toUnmodifiableList()));
        order.setOrderedAt(LocalDateTime.now());

        tx.execute(status -> {
           orderRepository.save(order);
           cartCleaner.clean(cart);
           return order;
        });
        return "ec/order/complete";
    }
}
