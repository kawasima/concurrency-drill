package net.unit8.examples.ec;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@WebAdapter
@Controller
@RequestMapping("/ec/order")
public class OrderController {
    @Autowired
    private OrderRepository orderRepository;

    private static final Long CUSTOMER_ID = 1L;

    @RequestMapping("/list")
    public String list(Model model) {
        final List<Order> orders = orderRepository.findAllByCustomerId(CUSTOMER_ID);
        model.addAttribute("orders", orders);
        return "ec/order/list";
    }
}
