package net.unit8.examples.ec;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@WebAdapter
@Controller
@RequestMapping("/ec/products")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StockKeepingUnitRepository stockKeepingUnitRepository;

    @Autowired
    private HttpSession session;

    @RequestMapping("/")
    public String index(@RequestParam(value = "q", required = false) String query,
                        @RequestParam(value = "p", required = false) Long pageNumber,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(Optional.ofNullable(pageNumber)
                .map(Long::intValue)
                .map(p -> p - 1)
                .orElse(0),
                10);

        Page<Product> products = Optional.ofNullable(query)
                .map(q -> "%" + q + "%")
                .map(q -> productRepository.findAllByQuery(q, pageRequest))
                .orElseGet(() -> productRepository.findAll(pageRequest));

        model.addAttribute("products", products);
        int totalPages = products.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "ec/products/list";
    }

    @RequestMapping("/{productId}")
    public String product(@PathVariable("productId") Long productId, Model model) {
        Product product = productRepository.findById(productId).orElseThrow();
        model.addAttribute("product", product);
        return "ec/products/detail";
    }

    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public String confirm(@Valid AddItemToCartForm form, BindingResult result,
                          Model model) {
        if (result.hasErrors()) {
            return product(form.getProductId(), model);
        }
        session.setAttribute("addItemToCartForm", form);
        Product product = productRepository.findById(form.getProductId()).orElseThrow();
        model.addAttribute("product", product);

        StockKeepingUnit sku = stockKeepingUnitRepository.findById(form.getSkuId()).orElseThrow();
        model.addAttribute("sku", sku);
        return "ec/products/confirm";
    }

}
