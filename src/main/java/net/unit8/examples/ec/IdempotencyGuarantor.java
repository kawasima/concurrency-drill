package net.unit8.examples.ec;

import java.util.Optional;

public interface IdempotencyGuarantor<T> {
     Optional<T> validate(String idempotentKey);
}
