package net.unit8.examples.ec;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProductRegistrationForm implements Serializable {
    private String name;
    private String description;
    private List<StockKeepingUnitForm> stockKeepingUnits;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<StockKeepingUnitForm> getStockKeepingUnits() {
        return stockKeepingUnits;
    }

    public void setStockKeepingUnits(List<StockKeepingUnitForm> stockKeepingUnits) {
        this.stockKeepingUnits = stockKeepingUnits;
    }

    public static class StockKeepingUnitForm implements Serializable {
        private String name;
        private BigDecimal price;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }
    }
}
