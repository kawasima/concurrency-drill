package net.unit8.examples.ec;

import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("nonIdempotencyGuarantor")
public class NonIdempotencyGuarantor<T> implements IdempotencyGuarantor<T> {
    @Override
    public Optional<T> validate(String idempotentKey) {
        return Optional.empty();
    }
}
