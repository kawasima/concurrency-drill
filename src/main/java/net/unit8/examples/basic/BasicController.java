package net.unit8.examples.basic;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.concurrent.atomic.AtomicInteger;

@WebAdapter
@Controller
@RequestMapping("/basic")
public class BasicController {
    private static final int TRIAL_COUNT = 1_000_000;

    @RequestMapping("/race_condition")
    public String raceCondition(Model model) throws InterruptedException {
        Counter counter = new Counter();
        Runnable counting = () -> {
            for (int x=0; x<TRIAL_COUNT; ++x) {
                counter.increment();
            }
        };
        Thread t1 = new Thread(counting);
        Thread t2 = new Thread(counting);
        long from = System.currentTimeMillis();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        long to = System.currentTimeMillis();
        model.addAttribute("count", counter.getCount());
        model.addAttribute("elapse", to - from);
        return "basic/raceCondition";
    }

    @RequestMapping("/race_condition/synchronized")
    public String raceConditionSync(Model model) throws InterruptedException {
        Counter counter = new Counter();
        Runnable counting = () -> {
            for (int x=0; x<TRIAL_COUNT; ++x) {
                synchronized (this) {
                    counter.increment();
                }
            }
        };
        Thread t1 = new Thread(counting);
        Thread t2 = new Thread(counting);
        long from = System.currentTimeMillis();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        long to = System.currentTimeMillis();
        model.addAttribute("count", counter.getCount());
        model.addAttribute("elapse", to - from);
        return "basic/raceCondition";
    }

    @RequestMapping("/race_condition/atomic")
    public String raceConditionAtomic(Model model) throws InterruptedException {
        AtomicCounter counter = new AtomicCounter();
        Runnable counting = () -> {
            for (int x=0; x<TRIAL_COUNT; ++x) {
                counter.increment();
            }
        };
        Thread t1 = new Thread(counting);
        Thread t2 = new Thread(counting);
        long from = System.currentTimeMillis();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        long to = System.currentTimeMillis();
        model.addAttribute("count", counter.getCount());
        model.addAttribute("elapse", to - from);
        return "basic/raceCondition";
    }

    private static class Counter {
        private int count = 0;
        public void increment() {
            count++;
        }
        public int getCount() {
            return count;
        }
    }

    private static class AtomicCounter {
        private final AtomicInteger count = new AtomicInteger(0);
        public void increment() {
            count.incrementAndGet();
        }
        public int getCount() {
            return count.intValue();
        }
    }
}
