package net.unit8.examples.bank;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    @Modifying
    @Query("update BankAccount a SET a.amount = :amount, a.version = :newVersion WHERE a.id = :accountId AND a.version = :currentVersion")
    Integer updateOptimistic(@Param("accountId") Long accountId,
                             @Param("amount") BigDecimal amount,
                             @Param("currentVersion") String currentVersion,
                             @Param("newVersion") String newVersion);
}
