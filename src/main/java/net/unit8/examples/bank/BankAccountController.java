package net.unit8.examples.bank;

import net.unit8.examples.stereotype.WebAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@WebAdapter
@Controller
@RequestMapping("/bank")
public class BankAccountController {
    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountLockRepository bankAccountLockRepository;

    @Autowired
    private TransactionTemplate tx;

    @Autowired
    private HttpSession session;

    private static final Long bankId = 1L;

    @RequestMapping("/")
    public String index(Model model) {
        tx.execute(status -> {
            BankAccount account = bankAccountRepository.findById(bankId).orElseGet(() -> {
                BankAccount newAccount = new BankAccount();
                newAccount.setId(bankId);
                newAccount.setAmount(new BigDecimal("10000"));
                newAccount.setVersion(UUID.randomUUID().toString());
                bankAccountRepository.save(newAccount);
                return newAccount;
            });
            model.addAttribute("account", account);
            return null;
        });
        return "bank/index";
    }

    @RequestMapping(value = "/reset", method = RequestMethod.POST)
    public String reset(Model model) {
        tx.execute(status -> {
            bankAccountRepository.findById(bankId).ifPresent(account -> {
                account.setAmount(new BigDecimal("10000"));
                account.setVersion(UUID.randomUUID().toString());
                bankAccountRepository.save(account);
                model.addAttribute("account", account);
                bankAccountLockRepository.findByAccountId(account.getId()).ifPresent(lock -> bankAccountLockRepository.delete(lock));
            });
            return null;
        });
        return "bank/index";
    }

    // ================================ 特になんの排他制御もしない ====================================
    @RequestMapping(value = "/transaction/no_guard", method = RequestMethod.GET)
    public String transactionNoGuardForm(Model model) {
        System.out.println(bankAccountRepository.findAll());
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();
        model.addAttribute("account", bankAccount);
        model.addAttribute(new AccountTransactionForm());
        return "bank/noGuardForm";
    }

    @RequestMapping(value = "/transaction/no_guard", method = RequestMethod.POST)
    public String transactionNoGuard(AccountTransactionForm form, Model model) {
        // 読み込み - ①
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();
        if (form.getAmount().compareTo(bankAccount.getAmount()) > 0) {
            model.addAttribute("amountIsTooBig", true);
            return transactionNoGuardForm(model);
        }
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException ignore) {
        }
        bankAccount.setAmount(bankAccount.getAmount().subtract(form.getAmount()));
        tx.execute(status -> {
            // 書き込み - ②
            // ①から②までの間に別のトランザクションが書き込むと、残高以上の出金が出来てしまうことになる。
            return bankAccountRepository.save(bankAccount);
        });
        return "redirect:/bank/";
    }

    // ================================ 楽観オフラインロック ====================================
    @RequestMapping(value = "/transaction/optimist", method = RequestMethod.GET)
    public String transactionOptimisticForm(Model model) {
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();
        model.addAttribute("account", bankAccount);
        AccountTransactionForm form = new AccountTransactionForm();
        form.setVersion(bankAccount.getVersion());
        model.addAttribute(form);
        return "bank/optimistForm";
    }

    @RequestMapping(value = "/transaction/optimist", method = RequestMethod.POST)
    public String transactionOptimistic(AccountTransactionForm form, Model model) {
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();
        if (form.getAmount().compareTo(bankAccount.getAmount()) > 0) {
            model.addAttribute("amountIsTooBig", true);
            return transactionOptimisticForm(model);
        }
        Integer updateCnt = tx.execute(status -> {

            return bankAccountRepository.updateOptimistic(bankId,
                    bankAccount.getAmount().subtract(form.getAmount()),
                    form.getVersion(),
                    UUID.randomUUID().toString());
        });
        if (updateCnt == null || updateCnt == 0) {
            model.addAttribute("updatedByAnotherTx", true);
            return transactionOptimisticForm(model);
        } else {
            return "redirect:/bank/";
        }
    }

    // ================================ 悲観オフラインロック ====================================
    @RequestMapping(value = "/transaction/pessimist", method = RequestMethod.GET)
    public String transactionPessimisticForm(Model model) {
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();
        return bankAccountLockRepository.findByAccountId(bankId).map(
                lock -> {
                    String accountLockId = (String) session.getAttribute("accountLockId");
                    if (accountLockId != null && Objects.equals(accountLockId, lock.getId())) {
                        model.addAttribute("account", bankAccount);
                        model.addAttribute(new AccountTransactionForm());
                        return "bank/pessimistForm";
                    } else {
                        return "bank/pessimistLock";
                    }
                }
        ).orElseGet(() -> {
            BankAccountLock lock = new BankAccountLock();
            lock.setBankAccount(bankAccount);
            tx.execute(status -> {
                return bankAccountLockRepository.save(lock);
            });
            model.addAttribute("account", bankAccount);
            model.addAttribute(new AccountTransactionForm());
            session.setAttribute("accountLockId", lock.getId());
            return "bank/pessimistForm";
        });
    }

    @RequestMapping(value = "/transaction/pessimist", method = RequestMethod.POST)
    public String transactionPessimistic(AccountTransactionForm form, Model model) {
        BankAccount bankAccount = bankAccountRepository.findById(bankId).orElseThrow();

        if (form.getAmount().compareTo(bankAccount.getAmount()) > 0) {
            model.addAttribute("amountIsTooBig", true);
            return transactionPessimisticForm(model);
        }
        bankAccount.setAmount(bankAccount.getAmount().subtract(form.getAmount()));
        BankAccountLock probe = new BankAccountLock();
        probe.setBankAccount(bankAccount);
        BankAccountLock bankAccountLock = bankAccountLockRepository.findByAccountId(bankId).orElseThrow();
        tx.execute(status -> {
            bankAccountLockRepository.delete(bankAccountLock);
            return bankAccountRepository.save(bankAccount);
        });
        session.removeAttribute("accountLockId");

        return "redirect:/bank/";
    }
}
