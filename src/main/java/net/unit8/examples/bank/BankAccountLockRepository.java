package net.unit8.examples.bank;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BankAccountLockRepository extends JpaRepository<BankAccountLock, Long> {
    @Query("SELECT l FROM BankAccountLock l WHERE l.bankAccount.id = :accountId")
    Optional<BankAccountLock> findByAccountId(@Param("accountId") Long accountId);
}
