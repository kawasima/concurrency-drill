package net.unit8.examples.bank;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountTransactionForm implements Serializable {
    private BigDecimal amount;
    private String version;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
