package net.unit8.examples.bank;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "bank_accounts")
public class BankAccount implements Serializable {
    @Id
    @Column(unique = true, nullable = false)
    private Long id;

    @Column
    private BigDecimal amount;

    @Column
    private String version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id=" + id +
                ", amount=" + amount +
                ", version='" + version + '\'' +
                '}';
    }
}
